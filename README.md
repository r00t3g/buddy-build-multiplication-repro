# Reproduction info:

## Prerequisites
1. Buddy On-premise installation
1. Gitlab Project with Buddy workspace or project key added as a Deploy Key
1. Project in Buddy that is bound to the repo in Gitlab as "Private Server" over SSH clone URL
1. A pipeline with a single action triggering the repo sync webhook (https://gitlab.com/r00t3g/buddy-build-multiplication-repro/-/blob/main/Triggerreposync.yml) - **replace sync webhook URL and email with real ones before importing**
1. A pipeline that is triggered on tag creation for 'refs/tags/*' wildcard (https://gitlab.com/r00t3g/buddy-build-multiplication-repro/-/blob/main/Testbuildpipeline.yml)

## Reproduction steps
1. Prepare a project in Buddy On-Prem as described above
1. Clone a repo locally
1. Use [tag-flood.sh](https://gitlab.com/r00t3g/buddy-build-multiplication-repro/-/blob/main/tag-flood.sh) script to generate and push 30 (or the desired amount) of tags
1. Wait for the builds to appear as repo sync pipeline executes every minute
1. Additionally tail the data/backend/logs/git-server/short-webhooks.log

## Note
I our case, flooding 26 tags produced almost 40 builds. Perhaphs additional workload handled concurrently by our installation affects such behavior, but all jobs are executed only on workers in our case, while primary only handles the Buddy's web UI and GIT server and other stuff it usually does. 

The setup and screenshots of redundant builds can be seen on the screenshots on the respective folder and exported pipelines' YAMLs in this repo.
